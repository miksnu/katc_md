# KATC_md

## Overview

- `BASE_URL`: http://api.wier.one

## Api Endpoints

### SendKATCLetterApi

- `KATC`에 편지를 작성합니다. (대상: 김예찬)
- Auth: `auth-no`
- Method: `POST`
- URL: `/katc/letter`
- Content Type: `application/x-www-form-urlencoded`
- Form

| Required | Key    | Type     | 설명                   |
| -------- | ------ | -------- | ---------------------- |
| True     | title  | str(20)  | 제목, 최소 5글자 이상  |
| True     | text   | str(800) | 내용, 최소 20글자 이상 |
| True     | writer | str(3)   | 작성자, 3글자 (실명)   |

- Success Response

```
{
    "result": {
        "code": 200,
        "msg": "성공적으로 작성되었습니다."
    }
}
```

### SendTheCampLetterApi

- `TheCamp`에 편지를 작성합니다. (대상: 김예찬)
- Auth: `auth-no`
- Method: `POST`
- URL: `/thecamp/letter`
- Content Type: `multipart/form-data`
- Form

| Required | Key    | Type      | 설명                                     |
| -------- | ------ | --------- | ---------------------------------------- |
| True     | title  | str(20)   | 제목, 최소 5글자 이상                    |
| True     | text   | str(2000) | 내용, 최소 20글자 이상, 줄바꿈 30번 이하 |
| True     | writer | str(3)    | 작성자, 3글자 (실명)                     |
| False    | image  | binary    | 이미지                                   |

- Success Response

```
{
    "result": {
        "code": 200,
        "msg": "성공적으로 작성되었습니다."
    }
}
```
